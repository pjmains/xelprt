import abc
import json
import os
from collections import namedtuple

from collections import defaultdict

import re
from datetime import datetime
from operator import itemgetter

import xlrd
from django.core.exceptions import ObjectDoesNotExist

from xelprt.forms import UploadFileForm
from . import models
from .models import Correction
from .workbook.factory import create_workbook
from .log import Logger

logger = Logger.get_logger()

FieldColumnMapping = namedtuple("ColumnMapping", ['column', 'field'])
CorrectionTuple = namedtuple("Correction", ["field", "bad_value", "options"])
CorrectableField = namedtuple("CorrectableField", ["name", "type"])

PICKLED_IMPORTER_DIR = os.path.join("uploads", "importer")


class MatchImporter:
    field_column_mappings = None
    correctable_fields = None
    import_type = None

    form = UploadFileForm

    def __init__(self, workbook, column_selections=None, sheet_index=None):
        if self.field_column_mappings is None:
            raise Exception("Importer must have a field_column_mappings property defined")
        if self.correctable_fields is None:
            raise Exception("Importer must have a correctable_fields property defined")
        if self.import_type is None:
            raise Exception("Importer must have a import_type property defined")

        self.workbook = workbook

        if column_selections is None:
            column_selections = {}
            for field_column_mapping in self.field_column_mappings:
                header_names = self.workbook.get_header_names(sheet_index)

                column_name = field_column_mapping.column
                column_index = header_names.index(column_name)
                column_selections[field_column_mapping.field] = column_index

        self.column_selections = column_selections

    @abc.abstractmethod
    def import_to_db(self, sheet_index):
        pass

    @classmethod
    def get_field_column_mappings(cls):
        return cls.field_column_mappings

    def get_cell_by_field(self, row, field):
        try:
            value = row[self.column_selections[field]]
        except KeyError:
            raise KeyError("Unable to find key {} in {}".format(field, set(self.column_selections.keys())))
        if isinstance(value, str):
            return value.strip()
        return value

    def get_all_data_rows(self, sheet_index=None):
        for row in self.workbook.get_n_rows(sheet_index=sheet_index, limit=None):
            if hasattr(row[0], "value"):
                yield [cell.value for cell in row]
            else:
                yield row

    def get_int_cell_by_field(self, row, field):
        value = self.get_cell_by_field(row=row, field=field)
        try:
            int_value = int(value)
        except ValueError:
            return None
        return int_value

    def get_bool_cell_by_field(self, row, field, none_values=None):
        value = self.get_cell_by_field(row=row, field=field)
        if isinstance(value, str):
            if value.upper() in ("T", "TRUE", "Y", "YES"):
                return True
            elif value.upper() in ("F", "FALSE", "N", "NO"):
                return False
        elif value in (1, 1.0):
            return True
        elif value in (0, 0.0):
            return False

        if none_values is None or value in none_values:
            return None

        raise ValueError("Cannot cast {} as boolean".format(value))

    def find_needed_corrections(self, sheet_index):
        corrections_dict = defaultdict(set)
        data_rows = self.get_all_data_rows(sheet_index=sheet_index)

        row_index = 0
        for data_row in data_rows:
            for field in self.correctable_fields:
                # If includes an object name as well a a table and field name in format Table:obj_name.field
                if ':' in field.name:
                    table_object_name, field_name = field.name.split(".")
                    table_name, object_name = table_object_name.split(":")
                # Otherwise, assume that we are using Table.field as our format
                else:
                    table_name, field_name = field.name.split(".")
                table_field_name = '{}.{}'.format(table_name, field_name)

                # Find the field value
                raw_value = self.get_cell_by_field(row=data_row, field=field.name)
                # If it's empty, skip it
                if isinstance(raw_value, str) and raw_value.strip() == '':
                    continue
                value = field.type(raw_value)

                try:
                    Correction.get_correct_value(cell_value=value, table_name=table_name, field_name=field_name)
                except ObjectDoesNotExist:
                    corrections_dict[table_field_name].add(value)
            row_index += 1

        logger.warn('Corrections found: {!r}'.format(corrections_dict))

        corrections = []
        for table_field_name, value_set in corrections_dict.items():
            table_name, field_name = table_field_name.split(".")
            options = self.find_options(table_name, field_name)
            for value in value_set:
                corrections.append(
                    CorrectionTuple(field=table_field_name, bad_value=value, options=options)
                )

        return corrections

    @staticmethod
    def find_options(table_name, field_name):
        table = getattr(models, table_name)
        values = table.objects.values('id', field_name)
        options = []
        for value in values:
            options.append({
                'id': value['id'],
                'value': value[field_name],
            })
        options.sort(key=itemgetter('value'))
        return options

    def save(self):
        if not os.path.isdir(PICKLED_IMPORTER_DIR):
            os.makedirs(PICKLED_IMPORTER_DIR)

        with open(os.path.join(PICKLED_IMPORTER_DIR, "{}.json".format(self.import_type)), "w") as json_file:
            json.dump(self.column_selections, json_file)

    @classmethod
    def load(cls):
        """Load the for this import type"""
        if not os.path.isdir(PICKLED_IMPORTER_DIR):
            os.makedirs(PICKLED_IMPORTER_DIR)

        with open(os.path.join(PICKLED_IMPORTER_DIR, "{}.json".format(cls.import_type))) as json_file:
            columns_selections = json.load(json_file)
        workbook = create_workbook(cls.import_type)
        importer_object = cls(workbook=workbook, column_selections=columns_selections)
        return importer_object

    @staticmethod
    def parse_name(name):
        tokens = [token for token in name.split() if not re.match('.*\d', token)]
        if '.' in tokens[-1] and len(tokens[-1]) == 2:
            tokens[-1] = tokens[-1][0]

        last_name, first_name, middle_initial = None, None, None
        if len(tokens) == 3:
            if len(tokens[-1]) == 1:
                middle_initial = tokens[-1]
                first_name = tokens[1]
                last_name = tokens[0].replace(",", "")
            elif "," in tokens[1]:
                tokens[1] = tokens[1].replace(",", "")
                last_name = " ".join(tokens[:2])
                first_name = " ".join(tokens[2])
            elif "," in tokens[0]:
                tokens[0] = tokens[0].replace(",", "")
                last_name = tokens[0]
                first_name = " ".join(tokens[1:3])
            else:
                last_name = tokens[0]
                first_name = tokens[1]
        elif len(tokens) == 2 or len(tokens) > 3:
            last_name = tokens[0].replace(",", "")
            first_name = tokens[1]
        elif len(tokens) == 1:
            last_name = tokens[0]
            first_name = tokens[0]
        else:
            raise ValueError("Cannot parse name {}".format(name))

        return first_name, middle_initial, last_name

    def get_date_from_cell_value(self, value):
        if value == '':
            return None

        value_as_datetime = datetime(*xlrd.xldate_as_tuple(float(value), self.workbook.book.datemode))
        return value_as_datetime
