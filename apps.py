from django.apps import AppConfig


class ImporterConfig(AppConfig):
    """The default AppConfig for admin which does autodiscovery."""

    name = 'xelprt'

    def ready(self):
        self.module.autodiscover()
