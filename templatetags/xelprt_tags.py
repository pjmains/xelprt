import re

from django import template

register = template.Library()

@register.filter(name="titleize")
def titleize(value):
    value = re.sub("[^a-zA-Z\d]+", " ", str(value))
    return value.title()
