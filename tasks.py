from celery import shared_task
from django.core.mail import send_mail

from project.settings import EMAIL_HOST_USER
from .registry import importer
from .log import Logger


logger = Logger.get_logger()


@shared_task
def run_import(email, import_type, sheet_number=None, label=None):
    import_class = importer.registry[import_type]
    logger.info("Initializing import for {}".format(import_class.__name__))
    if label is None:
        importer_obj = import_class()
    else:
        importer_obj = import_class(label=label)

    # noinspection PyBroadException
    try:
        logger.info("Running import_to_db")
        importer_obj.import_to_db(sheet_number)
        logger.info("Running import_to_db")
    except Exception as e:
        send_mail(
            '{} import failed'.format(import_type),
            '{} import failed. Your admin has been notified'.format(import_type),
            EMAIL_HOST_USER,
            [email],
            fail_silently=False,
        )

        send_mail(
            '{} import failed'.format(import_type),
            '{} import failed with the following exception\n\n{}'.format(import_type, e),
            EMAIL_HOST_USER,
            [EMAIL_HOST_USER],
            fail_silently=False,
        )
        return

    send_mail(
        '{} import successful'.format(import_type),
        '{} import successful'.format(import_type),
        EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )
