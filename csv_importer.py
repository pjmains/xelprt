from collections import defaultdict

from simple_importer import SimpleImporter
from log import Logger


logger = Logger.get_logger()


class CsvImporter(SimpleImporter):
    def __init__(self, csv_file_name, use_header=True):
        self.csv_file_name = csv_file_name
        self.use_header = use_header

        self.csv_file = None
        self.field_names = None

    def import_to_db(self, *args, **kwargs):
        logger.info("Entering import_to_db")

        # Isolate column->field relationships
        mapped_columns = self.config.dropna(subset=['column', 'field']).set_index('column')[['field']].copy()

        # Isolate default values
        self.default_values = self.config.dropna(subset=['field', 'value']).set_index('field')[['value']].copy()

        # Not let's import that data!
        self.field_mappings = defaultdict(list)
        for index in mapped_columns.index:
            field = mapped_columns.loc[index].field
            self.field_mappings[field].append(index)

        self.csv_file = open(self.csv_file_name, "w")

        self.field_names = list()
        used_fields = set()
        for field_name in self.config.field:
            if field_name not in used_fields:
                self.field_names.append(field_name)

        self.csv_file = open(self.csv_file_name, "w")

        if self.use_header:
            # Write "Field 1","Field 2","Etc." style header to our CSV file
            self.csv_file.write('"{}"'.format('","'.join(self.field_names)) + "\n")

        logger.info("Creating objects ...")
        for observation in self.next_observation:
            import_obj = self.model()
            self.create_obj(observation, import_obj)

        self.csv_file.close()

    def save_object(self, import_obj):
        obj_values = [import_obj[field_name] for field_name in self.field_names]
        self.csv_file.write('"{}"'.format('","'.join(obj_values)) + "\n")

    @staticmethod
    def set_attr(import_obj, field, field_value):
        import_obj[field] = field_value
