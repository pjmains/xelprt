import os
from glob import glob

from django import forms

from project import settings


def save_file(file_name, file_in_memory):
    with open(file_name, 'wb') as destination:
        for chunk in file_in_memory.chunks():
            destination.write(chunk)


class UploadFileForm(forms.Form):
    upload_file = forms.FileField()

    @staticmethod
    def save_files(import_type, request):
        upload_file = request.FILES['upload_file']

        file_name = request.FILES['upload_file'].name
        extension = file_name[file_name.rfind(".") + 1:]

        # Remove existing import files for this import type
        for import_file_path in glob(os.path.join(settings.UPLOADFILES_DIR, "{}.*".format(import_type))):
            os.remove(import_file_path)
        # Save our file
        destination_file_name = "{}.{}".format(import_type, extension)
        save_file(os.path.join(settings.UPLOADFILES_DIR, destination_file_name), upload_file)


class SimpleImportForm(forms.Form):
    config_file = forms.FileField()
    import_file = forms.FileField()

    @staticmethod
    def save_files(import_type, request):
        # Find import file name and extension
        import_file = request.FILES['import_file']
        import_file_name = import_file.name
        extension = import_file_name[import_file_name.rfind(".") + 1:]

        # Remove existing import files for this import type
        for import_file_path in glob(os.path.join(settings.UPLOADFILES_DIR, "{}.*".format(import_type))):
            os.remove(import_file_path)

        # Save import file
        import_file_path = os.path.join(settings.UPLOADFILES_DIR, "{}.{}".format(import_type, extension))
        save_file(import_file_path, import_file)

        # Save config file
        config_file = request.FILES['config_file']
        config_file_path = os.path.join(settings.UPLOADFILES_DIR, "{}.config.csv".format(import_type))
        save_file(config_file_path, config_file)
