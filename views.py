import json
from collections import defaultdict

import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from log import Logger

from . import models
from .registry import importer
from .simple_importer import SimpleImporter
from .workbook.factory import create_workbook
from . import tasks

logger = Logger.get_logger()
POST = "POST"


@login_required
def choose_import(request):
    context = {'import_types': sorted(importer.registry.keys())}
    logger.info(context)
    return render(request=request, template_name="xelprt/choose_import.html", context=context)


@login_required
def upload_spreadsheet(request, import_type):
    if request.method == POST:
        importer_class = importer.registry[import_type]
        form_class = importer_class.form
        upload_file_form = form_class(request.POST, request.FILES)

        if upload_file_form.is_valid():
            upload_file_form.save_files(import_type=import_type, request=request)

            if issubclass(importer_class, SimpleImporter):
                logger.info("Schedule import task for {}".format(importer_class.__name__))
                label = upload_file_form.cleaned_data.get("label", None)
                tasks.run_import.delay(request.user.email, import_type, label=label)
                return render(request=request, template_name="xelprt/import_success.html",
                              context={'email': request.user.email})
            else:
                file_name = request.FILES['upload_file'].name
                extension = file_name[file_name.rfind(".") + 1:]

                if extension == "csv":
                    return HttpResponseRedirect(reverse("match_fields", kwargs={'import_type': import_type}))
                else:
                    return HttpResponseRedirect(reverse("choose_sheet", kwargs={'import_type': import_type}))
        else:
            logger.warn("files; {}".format(request.FILES))
            logger.warn("Submitted Form is Invalid")
    else:
        form_class = importer.registry[import_type].form
        upload_file_form = form_class()

    context = {
        'import_type': import_type,
        'form': upload_file_form,
    }
    return render(request=request, template_name="xelprt/upload.html", context=context)


@login_required
def choose_sheet(request, import_type):
    """Choose Sheet from an Excel Workbook and Move on to Matching File"""

    # Create an empty form, and populate choices
    workbook = create_workbook(import_type)

    context = {
        'import_type': import_type,
        'summary': workbook.get_summary(),
    }

    return render(request=request, template_name="xelprt/choose_sheet.html", context=context)


@login_required
def match_fields(request, import_type, sheet_index=None):
    # Cast the page number from a str to an int
    if sheet_index is not None:
        sheet_index = int(sheet_index)

    book = create_workbook(import_type)
    sheet_width = book.get_sheet_width(sheet_index)

    first_five_rows = book.get_n_rows(sheet_index, limit=15, sheet_width=sheet_width)

    importer_class = importer.registry[import_type]

    context = {
        'rows': first_five_rows,
        'import_type': import_type,
        'column_headers': book.get_header_names(sheet_index=sheet_index),
        'column_mappings': importer_class.get_field_column_mappings(),
        'sheet_index': sheet_index
    }

    return render(request, template_name="xelprt/match_columns.html", context=context)


@login_required
def import_spreadsheet(request, import_type, sheet_index=None):
    if sheet_index is not None:
        sheet_index = int(sheet_index)

    workbook = create_workbook(import_type)

    importer_class = importer.registry[import_type]

    # If we posted the import, we made our column selections.
    if request.method == POST:
        # Figure out which column the user matched to each field
        column_selections = {}
        # Strip out noise from our form's POST dict
        for key, value in request.POST.items():
            # Grab only the fields that are set
            if not key.startswith('column_') or value == ['--']:
                continue

            # Column name is given as column_N, so int(N) is the column index
            column_index = int(key[key.find('_') + 1:])
            # Each selection can only have one value, so flatten from a list to a scalar
            column_selections[value] = column_index

        # Import the spreadsheet into the database
        importer_obj = importer_class(workbook=workbook, column_selections=column_selections)
        # Save in case we need to come back after making corrections
        importer_obj.save()
    # Otherwise, we're coming back from making corrections.
    elif request.method == "GET":
        importer_obj = importer_class.load()
    else:
        return HttpResponse(json.dumps(request.method), content_type="application/json")

    needed_corrections = importer_obj.find_needed_corrections(sheet_index=sheet_index)
    if len(needed_corrections) > 0:
        context = {
            'import_type': import_type,
            'sheet_index': sheet_index,
            'corrections': needed_corrections,
        }
        response = render(
            request=request, template_name="xelprt/make_corrections.html", context=context
        )
        return response

    # Run the import in the background
    tasks.run_import.delay(request.user.email, import_type, sheet_number=sheet_index)
    return render(request=request, template_name="xelprt/import_success.html", context={'email': request.user.email})


@login_required
def make_corrections(request, import_type, sheet_index):
    if sheet_index is not None:
        sheet_index = int(sheet_index)
    importer_class = importer.registry[import_type]
    importer_obj = importer_class.load()

    # Create dictionary of corrects
    corrections_nested_dict = defaultdict(dict)
    for key, value in request.POST.iteritems():
        if re.match(r'.*\.\d+$', key):
            field_key, correction_number = key.rsplit(".")
            corrections_nested_dict[correction_number][field_key] = value

    for correction_dict in corrections_nested_dict.values():
        # Find the class
        field_str = correction_dict['field']
        table_name, field_name = field_str.split(".")
        table = getattr(models, table_name)

        if correction_dict.get('add_new', None) == "on":
            # Find the value
            value = correction_dict['bad_value']
            # Create object and save
            table_obj = table()
            setattr(table_obj, field_name, value)
            table_obj.save()
        else:
            correction = models.Correction()
            correct_id = int(correction_dict['value_options'])
            correct_object = table.objects.get(id=correct_id)
            correction.correct_value = getattr(correct_object, field_name)
            correction.bad_value = correction_dict['bad_value']
            correction.field = correction_dict['field']
            # And ... save
            correction.save()

    # Find if we still need corrections, and start all over again if we do
    needed_corrections = importer_obj.find_needed_corrections(sheet_index=sheet_index)
    if len(needed_corrections) > 0:
        context = {
            'import_type': import_type,
            'sheet_index': sheet_index,
            'corrections': needed_corrections,
        }
        response = render(
            request=request, template_name="xelprt/make_corrections.html", context=context
        )
        return response

    # Otherwise, let's try to import again
    return HttpResponseRedirect(redirect_to="/import/{}/sheet/{}".format(import_type, sheet_index))
