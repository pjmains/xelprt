from .log import Logger


logger = Logger.get_logger()


class Importer:
    def __init__(self):
        logger.info("Initialize xelprt ...")
        self._registry = {}

    def register(self, name, cls):
        logger.info("Registering Importer {} {}".format(name, cls.__name__))
        self._registry[name] = cls

    @property
    def registry(self):
        return self._registry


importer = Importer()
