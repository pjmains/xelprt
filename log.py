import logging
import os
import sys

__author__ = "Peter Mains"


class Logger(object):
    loggers = {}
    DEFAULT_NAME = "xelprt"

    @classmethod
    def get_logger(cls, name=DEFAULT_NAME):
        if name in cls.loggers:
            return cls.loggers[name]

        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)

        logs_dir = 'logs'
        if not os.path.exists(logs_dir):
            # noinspection PyBroadException
            try:
                os.mkdir(logs_dir)
            except Exception:
                logs_dir = "."

        log_file_name = os.path.join(logs_dir, '{}.log'.format(name))
        cls.add_handler(logger=logger, file_name=log_file_name)
        cls.loggers[name] = logger

        return logger

    @staticmethod
    def add_handler(logger, file_name):
        formatter = logging.Formatter('[%(asctime)s] - [%(levelname)s] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

        stdout_handler = logging.StreamHandler(sys.stdout)
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(formatter)
        logger.addHandler(stdout_handler)

        file_handler = logging.FileHandler(filename=file_name)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)

    @classmethod
    def set_log_dir(cls, log_dir, name=DEFAULT_NAME):
        logger = cls.get_logger(name)

        handlers = logger.handlers.copy()

        for handler in handlers:
            logger.removeHandler(handler)

        file_name = os.path.join(log_dir, '{}.log'.format(name))
        print(file_name)

        cls.add_handler(logger, file_name=file_name)