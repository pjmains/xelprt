import sys

from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db import models


class Correction(models.Model):
    field = models.CharField(max_length=255)
    bad_value = models.CharField(max_length=255)
    correct_value = models.CharField(max_length=255)

    @classmethod
    def get_correct_value(cls, table_name, field_name, cell_value):
        if cell_value is None or (isinstance(cell_value, str) and cell_value.strip() == ''):
            return None

        module = sys.modules[cls.__module__]
        table = getattr(module, table_name)

        try:
            obj = table.objects.get(**{field_name: cell_value})
            value = getattr(obj, field_name)
            return value
        except ObjectDoesNotExist:
            try:
                correction = cls.objects.get(
                    field='{}.{}'.format(table_name, field_name), bad_value=cell_value
                )
            except MultipleObjectsReturned:
                corrections = cls.objects.filter(
                    field='{}.{}'.format(table_name, field_name), bad_value=cell_value
                )

                correction = corrections[0]
                for redundant_correction in corrections[1:]:
                    redundant_correction.delete()

            return cell_value.__class__(correction.correct_value)

    @classmethod
    def get_correct_object(cls, table_name, field_name, cell_value):
        """
        Given a table and field name, as well as a proposed value, find the object you're looking for
        :param table_name: Name of the table to search
        :param field_name: Name of the field that value corresponds to
        :param cell_value: The given field value
        :return: Correct object with field whose value is equivalent to the given value
        """

        if cell_value == '':
            return None

        # Find the correct value
        correct_value = cls.get_correct_value(table_name, field_name, cell_value)
        # Retrieve the correct object
        module = sys.modules[cls.__module__]
        table = getattr(module, table_name)
        try:
            correct_object = table.objects.get(**{field_name: correct_value})
        except ObjectDoesNotExist:
            raise Exception("Unable to find correct value in {}.{} for {}".format(
                table_name, field_name, cell_value
            ))
        return correct_object

    def __str__(self):
        return "{}: {!r} -> {!r}".format(self.field, self.bad_value, self.correct_value)
