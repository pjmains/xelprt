import os
import pandas as pd
import numpy as np
from datetime import datetime
from collections import defaultdict

from project import settings
from .forms import SimpleImportForm
from .log import Logger


logger = Logger.get_logger()


class SimpleImporter:
    import_type = None
    model = None
    form = SimpleImportForm

    def __init__(self, *args, **kwargs):
        self.default_values = None
        self.field_mappings = None

        self._data_frame_iterator = None
        self._config = None

    @property
    def config(self):
        if self._config is None:
            config_path = os.path.join(settings.UPLOADFILES_DIR, "{}.config.csv".format(self.import_type))
            self._config = pd.read_csv(config_path)

        return self._config

    @config.setter
    def config(self, config_file_path):
        self._config = pd.read_csv(config_file_path, dtype=np.str)

    @property
    def next_observation(self):
        file_path = os.path.join(settings.UPLOADFILES_DIR, "{}.csv".format(self.import_type))

        # Get dtypes for get_csv
        typed_columns = self.config.dropna(subset=['column', 'type']).set_index('column')[['type']].copy()
        mapped_dtypes = {index: eval(typed_columns.loc[index].type) for index in typed_columns.index}

        used_columns = list(self.config.dropna(subset=['column', 'field']).column.unique())
        try:
            logger.info("Creating data frame")
            # Read in the data we want to import as an iterator
            df_iterator = pd.read_csv(
                file_path, dtype=mapped_dtypes, usecols=used_columns, chunksize=1000, iterator=True
            )
        except pd.errors.ParseError:
            df_iterator = pd.read_excel(
                file_path, dtype=mapped_dtypes, usecols=used_columns, chunksize=1000, iterator=True
            )

        # Now concatenate our chunks into a single data frame
        self._data_frame_iterator = df_iterator  # pd.concat(df_iterator, ignore_index=True)

        for df in self._data_frame_iterator:
            for _, observation in df.iterrows():
                yield observation

    def import_to_db(self, *args, **kwargs):
        logger.info("Entering import_to_db")

        # Isolate column->field relationships
        mapped_columns = self.config.dropna(subset=['column', 'field']).set_index('column')[['field']].copy()

        # Isolate default values
        self.default_values = self.config.dropna(subset=['field', 'value']).set_index('field')[['value']].copy()

        # Not let's import that data!
        self.field_mappings = defaultdict(list)
        for index in mapped_columns.index:
            field = mapped_columns.loc[index].field
            self.field_mappings[field].append(index)

        logger.info("Creating objects ...")
        for observation in self.next_observation:
            import_obj = self.model()
            self.create_obj(observation, import_obj)

    def create_obj(self, observation, import_obj):
        for field, column_list in self.field_mappings.items():
            if len(column_list) > 1:
                field_value = ""
                for column_name in column_list:
                    column_value = observation[column_name]
                    if not (type(column_value) is float and np.isnan(column_value)):
                        field_value += column_value + " "
                field_value = field_value.strip()
                self.set_attr(import_obj, field, field_value)
            else:
                # Find what the column value is
                column_name = column_list[0]
                column_value = observation[column_name]
                # Don't both setting NaN values
                try:
                    if column_value is None or (type(column_value) is float and np.isnan(column_value)):
                        continue
                except TypeError:
                    raise TypeError("Unable to apply numpy.isnan to {}".format(type(column_value)))

                self.set_field(import_obj, field, column_value)

        for default_field, default_value in self.default_values.iterrows():
            self.set_field(import_obj, default_field, default_value.value)

        self.save_object(import_obj)

    @staticmethod
    def save_object(import_obj):
        import_obj.save()

    @staticmethod
    def set_attr(import_obj, field, field_value):
        setattr(import_obj, field, field_value)

    def set_field(self, import_obj, field, column_value):
        field_type = self.model._meta.get_field(field).get_internal_type()

        # Set field value based on the field's internal type
        if field_type == 'DateField':
            date_value = datetime.strptime(column_value, '%m/%d/%Y')
            setattr(import_obj, field, date_value)
        elif field_type == 'CharField':
            setattr(import_obj, field, column_value)
        elif field_type == 'IntegerField':
            setattr(import_obj, field, int(column_value))
        elif field_type in ('BooleanField', 'NullBooleanField'):
            # Parse the boolean value from string
            if column_value.upper() in ("Y", "YES", "T", "TRUE", "A"):
                bool_value = True
            elif column_value.upper() in ("N", "NO", "F", "FALSE", "D"):
                bool_value = False
            elif column_value == "":
                bool_value = None
            else:
                raise ValueError("Unrecognized boolean str for {}: {}".format(field, column_value))

            setattr(import_obj, field, bool_value)
