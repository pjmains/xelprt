from collections import namedtuple

__author__ = "Peter Mains"

SheetSummary = namedtuple("SheetSummary", ["index", "name", "summary"])


class Workbook(object):
    """Generic spreadsheet reader"""
    def __init__(self, book):
        self.book = book

    def get_sheet_width(self, sheet_index):
        """Find width of worksheet by finding the maximum length of a row in the worksheet"""

        worksheet = self.book.sheet_by_index(sheet_index)

        sheet_width = 0
        for row_index in range(worksheet.nrows):
            sheet_width = max(sheet_width, self.get_row_width(sheet_index, row_index=row_index))

        return sheet_width

    def get_row_width(self, sheet_index, row_index):
        """Find the length of the given row, excluding empty columns at the end"""

        sheet = self.book.sheet_by_index(sheet_index)
        row = sheet.row(row_index)
        row_length = len(row)

        # Ignore values whose type is 'empty' or values that are empty strings (e.g. u'')
        while (row[row_length-1].ctype == 0 or str(row[row_length-1].value).strip() == '') and row_length > 1:
            row_length -= 1

        return row_length

    def get_n_rows(self, sheet_index, sheet_width=None, limit=5):
        """Get first N lines in spreadsheet. Start at the first line whose width matches our sheet width"""

        # Get worksheet
        worksheet = self.book.sheet_by_index(sheet_index)
        if worksheet.nrows == 0:
            return []

        # If there is no limit set, we set it to the number of rows in the spreadsheet
        if limit is None:
            limit = worksheet.nrows

        # If the width of the sheet is not provided, we provide one
        if sheet_width is None:
            sheet_width = self.get_sheet_width(sheet_index)

        # Start looking for the first row (usually a header row) at row 0
        start_row_index = 0
        # Iterate so long as we have a row that is shorter than the width of the spreadsheet
        while self.get_row_width(sheet_index, row_index=start_row_index) < sheet_width:
            start_row_index += 1

        # Start with the end row being the same as the start row
        end_row_index = start_row_index

        # If there are fewer remaining rows than the current limit allows, decrease the limit
        limit = min(limit, worksheet.nrows - start_row_index)

        # Keep going while the first cell of the next row is non-empty and the
        while (end_row_index - start_row_index + 1 < limit) and self.get_row_width(sheet_index, end_row_index+1):
            # Iterate and look at the first cell of the next row
            end_row_index += 1

        # And now we grab the rows that we want
        n_rows = [worksheet.row(i) for i in range(start_row_index, end_row_index+1)]

        return n_rows

    def get_header_names(self, sheet_index):
        header_row = self.get_n_rows(sheet_index=sheet_index, limit=1)[0]
        column_names = [cell.value for cell in header_row]

        return column_names

    def get_summary(self):
        summary = []
        for i in range(self.get_sheet_count()):
            first_five_rows = self.get_n_rows(sheet_index=i)
            name = self.book.sheet_by_index(i).name
            summary.append(SheetSummary(index=i, name=name, summary=first_five_rows))

        return summary

    def get_sheet_count(self):
        return self.book.nsheets
