import os
from glob import glob

from project.settings import UPLOADFILES_DIR

from .csv_book import CsvBook
from .xls_book import XlsBook


def create_workbook(import_type):
    """
    :param: str file_path
    """

    import_file_paths = glob(os.path.join(UPLOADFILES_DIR, import_type + ".*"))

    if len(import_file_paths) == 0:
        raise ValueError("Unable to upload file for {}".format(import_type))
    elif len(import_file_paths) > 1:
        raise ValueError("Multiple {} files detected".format(import_type))

    file_path = import_file_paths[0]

    if file_path.endswith("csv"):
        return CsvBook(import_type)

    if file_path.endswith("xls"):
        return XlsBook(import_type)

    raise ValueError("Unable to identify file extension {}".format(file_path[file_path.rfind(".")+1:]))
