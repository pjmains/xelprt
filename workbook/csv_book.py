import os

import pandas as pd

from project.settings import UPLOADFILES_DIR
from xelprt.workbook import Workbook


class CsvBook(Workbook):
    """Generic spreadsheet reader"""

    def __init__(self, import_type):
        import_path = os.path.join(UPLOADFILES_DIR, "{}.{}".format(import_type, "csv"))
        book = pd.read_csv(import_path)

        super(CsvBook, self).__init__(book)

    def get_sheet_width(self, *args, **kwargs):
        """Find width of worksheet by finding the maximum length of a row in the worksheet"""

        return len(self.book.columns)

    def get_row_width(self, *args, **kwargs):
        """Find the length of the given row, excluding empty columns at the end"""

        return self.get_sheet_width()

    def get_n_rows(self, *args, **kwargs):
        """Get first N lines in spreadsheet. Start at the first line whose width matches our sheet width"""

        limit = 5
        if "limit" in kwargs:
            limit = kwargs['limit']

        # Get worksheet
        if len(self.book) == 0:
            return []

        # If there is no limit set, we set it to the number of rows in the spreadsheet
        if limit is None or limit > len(self.book):
            return self.book.values

        return self.book.iloc[:limit].values

    def get_header_names(self, *args, **kwargs):
        return list(self.book.columns)

    def get_sheet_count(self):
        return 1
