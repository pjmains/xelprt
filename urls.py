from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.choose_import, name="choose_import"),
    url(r'^upload/(?P<import_type>[\w_]+)/?$', views.upload_spreadsheet, name="upload_spreadsheet"),
    url(r'^choose_sheet/(?P<import_type>[\w_]+)/?$', views.choose_sheet, name="choose_sheet"),
    url(r'^match_field_names/(?P<import_type>[\w_]+)/?$', views.match_fields, name="match_fields"),
    url(
        r'^match_field_names/(?P<import_type>[\w_]+)/sheet/(?P<sheet_index>\d+)/?$',
        views.match_fields, name="match_fields"
    ),
    url(r'^import/(?P<import_type>[\w_]+)/?$', views.import_spreadsheet, name="import_spreadsheet"),
    url(
        r'^import/(?P<import_type>[\w_]+)/sheet/(?P<sheet_index>\d+)/?$',
        views.import_spreadsheet, name="import_spreadsheet"
    ),
    url(
        r'^correct/(?P<import_type>[\w_]+)/sheet/(?P<sheet_index>\d+)/?$',
        views.make_corrections, name="make_corrections"
    ),
]
