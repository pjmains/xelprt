from django.utils.module_loading import autodiscover_modules
from .registry import importer


def autodiscover():
    autodiscover_modules('import_types', register_to=importer)


default_app_config = 'xelprt.apps.ImporterConfig'
